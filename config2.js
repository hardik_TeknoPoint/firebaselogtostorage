import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyBQ0HlAtofA52OSxFk-GPGiAN-roekuTSU',
  authDomain: 'fir-logger-4a92a.firebaseapp.com',
  projectId: 'fir-logger-4a92a',
  storageBucket: 'fir-logger-4a92a.appspot.com',
  messagingSenderId: '1001089480883',
  appId: '1:1001089480883:web:9b8825c7e658aba03118c6',
};
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}
export {firebase};
