import React, {Component} from 'react';
import {FileLogger} from 'react-native-file-logger'; // logging data to mobile cache with this npm package.
import {firebase} from '../../config2'; /// firebase configuration

FileLogger.configure(); /// configuring file logger to sysnc with logging system. in react native. by calling this function it stars storing data in mobile cashe folder
import RNFS from 'react-native-fs'; //// react native file system package
import {
  Button,
  PermissionsAndroid,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';

const requestStoragePermission = async () => {
  // place where file logger stores log files.
  const loggingPath = await FileLogger.getLogFilePaths();
  console.log('loggingPath==----', loggingPath); //"/storage/emulated/0/Android/data/com.firebase_logger/cache/logs/com.firebase_logger-latest.log"
  try {
    // at frist clling this func we r requiseting for permition.
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Permisiion Allowance',
        message: 'You are allowing the permission to store the file.',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );

    // checking for permision if "granted" then we can store file into download directory or into external storage.
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      const DownLoadPath = RNFS.DownloadDirectoryPath + '/joke.txt';
      const fileContent =
        "Why do programmers wear glasses? \n They can't C#!  \n Hardikh  \n Chinmay";
      // storing file data in utf8 format.
      await RNFS.writeFile(DownLoadPath, fileContent, 'utf8');
    } else {
      console.log('file write permission denied');
    }
  } catch (err) {
    console.warn(err);
  }
};

/// read file func we call it when we want to  store data into firebase/store to save file there.
const readFileFunction = async () => {
  // reading log file from this path.
  const readFilePath =
    '/storage/emulated/0/Android/data/com.firebase_logger/cache/logs/com.firebase_logger-latest.log';

  try {
    const responce = await RNFS.readFile(readFilePath, 'utf8');
    // firebase
    //   .storage()
    //   .ref('/logFiles')
    //   .child('newLog6.log')
    //   .put(new File([responce], 'sample-text.txt', {type: 'text/plain'}), {
    //     type: 'text/plain',
    //   });
  } catch (error) {
    console.log(
      'logging from reading file from storage---***************************',
      error,
    );
  }
};

// getting file does not working right now.

class MyWriteFile extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.item}>Try permissions</Text>
        <Button title="write permissions" onPress={requestStoragePermission} />
        {/* <Button title="read File" onPress={readFileFunction} /> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  item: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
export default MyWriteFile;
