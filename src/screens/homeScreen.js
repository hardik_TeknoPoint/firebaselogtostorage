import {Text, View} from 'react-native';
import React, {Component} from 'react';
import MyWriteFile from '../components/myWriteFile';

//    ****************************************************************************************************************************
////////                 main file is "MyWriteFile.js"
//    ****************************************************************************************************************************

export default class HomeScreen extends Component {
  render() {
    return (
      <View style={{width: '100%', height: 300}}>
        <MyWriteFile />
        <Text>HomeScreen</Text>
      </View>
    );
  }
}
